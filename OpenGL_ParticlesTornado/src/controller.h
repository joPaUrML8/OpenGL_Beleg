#pragma once
#include <vector>
#include <algorithm>
#include "utility.h"


namespace controller {

	GLuint shaderProgram = 0;
	GLint positionLocation, colorLocation, xyzsLocation;

	GLuint vao;
	GLuint verticesVbo;

	// vertices for the particle
	static const GLfloat vertexData[] = {
		 -0.5f, -0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		 -0.5f, 0.5f, 0.0f,
		 0.5f, 0.5f, 0.0f,
	};

	GLuint locationVbo;

	int width, height;

	// definition in Init() since I need to get the actual window size first
	glm::mat4 projection;

	// Camera matrix
	glm::mat4 view = glm::lookAt(
		glm::vec3(0, 10, 20), // eye
		glm::vec3(0, 0, 0), // target
		glm::vec3(0, 1, 0)  // up-vector
	);

	glm::mat4 vp;

	GLuint CameraRight_worldspace_ID;
	GLuint CameraUp_worldspace_ID;
	GLuint vpLocation;

	GLuint texture, textureLocation;

	double lastTime = 0, currentTime, timeDelta;

	// CPU representation of a particle
	struct Particle {
		glm::vec3 pos;
		float size;
		float life; // Remaining life of the particle. if <0 : dead and unused.
		float cameradistance; // *Squared* distance to the camera. if dead : -1.0f
		float radius;
		float randomOffset;

		bool operator<(const Particle& that) const {
			// Sort in reverse order : far particles drawn first.
			return this->cameradistance > that.cameradistance;
		}
	};

	const int MAXPARTICLES = 100000;
	Particle ParticlesContainer[MAXPARTICLES];
	int LastUsedParticle = 0;
	int ParticlesCount;

	static GLfloat* particleDataArray = new GLfloat[MAXPARTICLES * 4];

	// Finds a Particle in ParticlesContainer which isn't used yet.
	// (i.e. life < 0);
	int FindUnusedParticle() {

		for (int i = LastUsedParticle; i < MAXPARTICLES; i++) {
			if (ParticlesContainer[i].life <= 0) {
				LastUsedParticle = i;
				return i;
			}
		}

		for (int i = 0; i < LastUsedParticle; i++) {
			if (ParticlesContainer[i].life <= 0) {
				LastUsedParticle = i;
				return i;
			}
		}
		std::cout << "Particles full" << std::endl;
		return 0; // All particles are taken, override the first one
	}

	void SortParticles() {
		std::sort(&ParticlesContainer[0], &ParticlesContainer[MAXPARTICLES]);
	}

	void Init(GLFWwindow* window) {
		glClearColor(0.9f, 0.9f, 0.9f, 0.0f);
		
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		GLuint vertexShader, fragmentShader;
		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		utility::CompileShader(vertexShader, "resources/vertex.glsl");
		utility::CompileShader(fragmentShader, "resources/fragment.glsl");

		shaderProgram = glCreateProgram();
		glAttachShader(shaderProgram, vertexShader);
		glAttachShader(shaderProgram, fragmentShader);
		utility::LinkShaderProgram(shaderProgram);

		glDetachShader(shaderProgram, vertexShader);
		glDetachShader(shaderProgram, fragmentShader);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		//----------------------------- buffer init start --------------------------------------

		positionLocation = glGetAttribLocation(shaderProgram, "vPosition");

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		// buffer for the vertex data
		glGenBuffers(1, &verticesVbo);
		glBindBuffer(GL_ARRAY_BUFFER, verticesVbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_DYNAMIC_DRAW);

		// vao for position data
		glEnableVertexAttribArray(positionLocation);
		glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

		// buffer for particle location
		glGenBuffers(1, &locationVbo);
		glBindBuffer(GL_ARRAY_BUFFER, locationVbo);
		glBufferData(GL_ARRAY_BUFFER, MAXPARTICLES * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);

		xyzsLocation = glGetAttribLocation(shaderProgram, "xyzs");

		// vao for indivual particle info
		glEnableVertexAttribArray(xyzsLocation);
		glVertexAttribPointer(xyzsLocation, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);
		
		glVertexAttribDivisor(positionLocation, 0); // particles vertices : always reuse the same 4 vertices -> 0
		glVertexAttribDivisor(xyzsLocation, 1); // positions : one per quad (its center)                 -> 1

		//----------------------------- buffer init end --------------------------------------

		// get current window size in pixel
		glfwGetFramebufferSize(window, &width, &height);

		// create the projection matrix
		projection = glm::perspective(glm::radians(45.0f), (float)width / (float) height, 0.1f, 100.0f);

		vp = projection * view;

		// all uniform variables of vertex shader
		CameraRight_worldspace_ID = glGetUniformLocation(shaderProgram, "CameraRight_worldspace");
		CameraUp_worldspace_ID = glGetUniformLocation(shaderProgram, "CameraUp_worldspace");
		vpLocation = glGetUniformLocation(shaderProgram, "VP");

		// texture id in fragment shader
		textureLocation = glGetUniformLocation(shaderProgram, "textureSampler");

		texture = loadDDS("resources/smoke.DDS");
	}

	void SpawnParticles(float numberOfParticles = 10000) {
		int newparticles = (int)(timeDelta * numberOfParticles);
		if (newparticles > (int)(0.016f * 10000.0))
			newparticles = (int)(0.016f * 10000.0);

		for (int i = 0; i < newparticles; i++) {
			int particleIndex = FindUnusedParticle();
			ParticlesContainer[particleIndex].life = 7.0f; // This particle will live 5 seconds.

			ParticlesContainer[particleIndex].pos = glm::vec3(0);

			ParticlesContainer[particleIndex].size = (rand() % 1000) / 2000.0f + 0.1f;

			ParticlesContainer[particleIndex].radius = 0;
			ParticlesContainer[particleIndex].randomOffset = rand() % 10;
		}
	}

	void ParticleLogic() {

		glm::vec3 CameraPosition(glm::inverse(view)[3]);

		// Simulate all particles
		ParticlesCount = 0;
		for (int i = 0; i < MAXPARTICLES; i++) {

			Particle& p = ParticlesContainer[i]; // shortcut

			if (p.life > 0.0f) {

				// Decrease life
				p.life -= timeDelta;
				
				if (p.life > 0.0f) {

					// Move particles in circular fashion
					p.radius += timeDelta;
					p.pos = glm::vec3(glm::sin(currentTime), 0, 0) + glm::vec3((p.radius / 2) * glm::cos(p.life * 2 + p.randomOffset), p.radius, (p.radius / 2) * glm::sin(p.life * 2 + p.randomOffset));
					p.cameradistance = glm::length2(p.pos - CameraPosition);

					// Fill the GPU buffer
					particleDataArray[4 * ParticlesCount + 0] = p.pos.x;
					particleDataArray[4 * ParticlesCount + 1] = p.pos.y;
					particleDataArray[4 * ParticlesCount + 2] = p.pos.z;
					particleDataArray[4 * ParticlesCount + 3] = p.size;

				}
				else {
					// Particles that just died will be put at the end of the buffer in SortParticles();
					p.cameradistance = -1.0f;
				}

				ParticlesCount++;

			}
		}

		SortParticles();
	}

	void Display() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		currentTime= glfwGetTime();
		timeDelta = currentTime - lastTime;
		lastTime = currentTime;

		SpawnParticles(1000);
		
		ParticleLogic();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glUseProgram(shaderProgram);

		//particle stuff
		glBindVertexArray(vao);

		glUniform3f(CameraRight_worldspace_ID, view[0][0], view[1][0], view[2][0]);
		glUniform3f(CameraUp_worldspace_ID, view[0][1], view[1][1], view[2][1]);

		
		glUniformMatrix4fv(vpLocation, 1, GL_FALSE, glm::value_ptr(vp));
		
		glBindBuffer(GL_ARRAY_BUFFER, locationVbo);
		glBufferData(GL_ARRAY_BUFFER, MAXPARTICLES * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, ParticlesCount * sizeof(GLfloat) * 4, particleDataArray);

		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, ParticlesCount);
	}

	void Dispose() {
		glUseProgram(0);
		glDeleteProgram(shaderProgram);

		glDeleteBuffers(1, &verticesVbo);
		glDeleteBuffers(1, &locationVbo);
		glDeleteVertexArrays(1, &vao);
		glDeleteTextures(1, &texture);
	}

}