#pragma once
#include "libs.h"
#include "textureLoader.h"
#include <fstream>
#include <vector>

/*
* Hilfsfunktionen zum Kompilieren und Linken von Shadern.
* Siehe https://elminster.hs-mittweida.de/doku.php?id=gpu:shader_drawcall#shader-spezifikation
*/

namespace utility {

	bool CompileShader(GLuint shader, const char* filename) {
		std::ifstream stream(filename);
		if (stream.fail()) {
			std::cerr << "Error opening file " << filename << std::endl;
			return false;
		}
		std::string shaderSource((std::istreambuf_iterator<char>(stream)),
			std::istreambuf_iterator<char>());
		stream.close();

		const GLchar* source[]{ shaderSource.c_str() };

		glShaderSource(shader, 1, source, nullptr);
		glCompileShader(shader);

		GLint status = 0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if (status == GL_FALSE) {
			GLint logSize = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
			std::vector<GLchar> infoLog;
			infoLog.resize(logSize);
			glGetShaderInfoLog(shader, logSize, &logSize, &infoLog[0]);

			std::cerr << "Error compiling shader code from file " <<
				filename << ": " << std::endl;
			std::cerr << infoLog.data() << std::endl;

			return false;
		}
		return true;
	}

	bool LinkShaderProgram(GLuint program) {
		GLint status = 0;
		glLinkProgram(program);
		glGetProgramiv(program, GL_LINK_STATUS, &status);
		if (status == GL_FALSE) {
			GLint logSize = 0;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logSize);
			std::vector<GLchar> infoLog;
			infoLog.resize(logSize);
			glGetProgramInfoLog(program, logSize, &logSize, &infoLog[0]);

			std::cerr << "Error linking shader program (handle " << program << "): " << std::endl;
			std::cerr << infoLog.data() << std::endl;

			return false;
		}
		return true;
	}

	void PrintOpenGLInformation() {
		std::cout << "OpenGL Vendor: " << glGetString(GL_VENDOR) << std::endl;
		std::cout << "OpenGL Renderer: " << glGetString(GL_RENDERER) << std::endl;
		std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
		std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
	}

} // end of namespace