#version 430

in vec4 particleColor;

out vec4 vFragColor;

void main(void) {
	vFragColor = particleColor;
}