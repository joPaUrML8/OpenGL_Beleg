#version 430

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec4 xyzs;
layout(location = 2) in vec4 color;

// Values that stay constant for the whole mesh.
uniform vec3 CameraRight_worldspace;
uniform vec3 CameraUp_worldspace;
uniform mat4 VP; // Model-View-Projection matrix, but without the Model (the position is in BillboardPos; the orientation depends on the camera)

out vec4 particleColor;

void main(void) {
	float particleSize = xyzs.w; // because we encoded it this way.
	vec3 particleCenter_wordspace = xyzs.xyz;
	
	vec3 vertexPosition_worldspace = 
		particleCenter_wordspace
		+ CameraRight_worldspace * vPosition.x * particleSize
		+ CameraUp_worldspace * vPosition.y * particleSize;


	// Output position of the vertex
	gl_Position = VP * vec4(vertexPosition_worldspace, 1.0f);

	particleColor = color;
}