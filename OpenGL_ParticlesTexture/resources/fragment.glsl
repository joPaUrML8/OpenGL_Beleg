#version 430

in vec2 UV;
in vec4 particleColor;

out vec4 vFragColor;

uniform sampler2D textureSampler;

void main(void) {
	vFragColor = texture(textureSampler, UV) * particleColor;
}