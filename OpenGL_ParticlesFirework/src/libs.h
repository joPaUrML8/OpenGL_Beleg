#pragma once

#ifdef _WIN32
#include <Windows.h>
#endif

#include <iterator>
#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>


#include <GLFW/glfw3.h>