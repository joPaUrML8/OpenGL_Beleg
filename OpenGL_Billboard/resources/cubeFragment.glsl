#version 430

uniform vec3 vColor;

layout (location = 0) out vec3 vFragColor;

void main(void) {
	vFragColor = vec3(1, 1, 1);
}