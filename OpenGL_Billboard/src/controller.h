#pragma once
#include <vector>
#include "utility.h"


namespace controller {

	GLuint shaderProgram = 0;
	GLint positionLocation, colorLocation;

	GLuint vao = 0;
	GLuint vbo = 0;

	static const GLfloat vertexData[] = {
		 -0.5f, -0.5f, 0.0f,
		 0.5f, -0.5f, 0.0f,
		 -0.5f, 0.5f, 0.0f,
		 0.5f, 0.5f, 0.0f,
	};

	int width, height;

	// definition in Init() since I need to get the actual window size first
	glm::mat4 projection;

	// Camera matrix
	glm::mat4 view;

	glm::mat4 vp;

	GLuint CameraRight_worldspace_ID;
	GLuint CameraUp_worldspace_ID;
	GLuint vpLocation;
	GLuint BillboardPosID;
	GLuint BillboardSizeID;

	double lastTime = 0, currentTime, timeDelta;

	// cube stuff
	static const GLfloat cubeVertexData[] = {
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	-1.0f,-1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f,-1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f
	};
	GLuint cubeProgram = 1;
	GLuint cubeMVPLocation;
	GLuint cubeVao = 1;
	GLuint cubeVbo = 1;
	GLuint cubePositionLocation;


	void RotateCameraAroundObject() {
		glm::vec3 newPosition = glm::vec3(4 * glm::cos(currentTime), 1, 4 * glm::sin(currentTime));

		std::cout << newPosition.x << "x, " << newPosition.y << "y, " << newPosition.z << "z" << std::endl;

		view = glm::lookAt(
			newPosition, // eye
			glm::vec3(0, 0, 0), // target
			glm::vec3(0, 1, 0)  // up-vector
		);

		vp = projection * view;
	}

	void Init(GLFWwindow* window) {
		glClearColor(0.1F, 0.1F, 0.1F, 1.0F);
		
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);

		GLuint vertexShader, fragmentShader;
		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		utility::CompileShader(vertexShader, "resources/vertex.glsl");
		utility::CompileShader(fragmentShader, "resources/fragment.glsl");

		shaderProgram = glCreateProgram();
		glAttachShader(shaderProgram, vertexShader);
		glAttachShader(shaderProgram, fragmentShader);
		utility::LinkShaderProgram(shaderProgram);

		glDetachShader(shaderProgram, vertexShader);
		glDetachShader(shaderProgram, fragmentShader);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		positionLocation = glGetAttribLocation(shaderProgram, "vPosition");
		colorLocation = glGetUniformLocation(shaderProgram, "vColor");

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(positionLocation);
		glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		
		// get current window size in pixel
		glfwGetFramebufferSize(window, &width, &height);

		// create the projection matrix
		projection = glm::perspective(glm::radians(45.0f), (float)width / (float) height, 0.1f, 100.0f);

		vp = projection * view;

		// Vertex shader
		CameraRight_worldspace_ID = glGetUniformLocation(shaderProgram, "CameraRight_worldspace");
		CameraUp_worldspace_ID = glGetUniformLocation(shaderProgram, "CameraUp_worldspace");
		vpLocation = glGetUniformLocation(shaderProgram, "VP");
		BillboardPosID = glGetUniformLocation(shaderProgram, "BillboardPos");
		BillboardSizeID = glGetUniformLocation(shaderProgram, "BillboardSize");

		// cube stuff
		GLuint cubeVertexShader, cubeFragmentShader;
		cubeVertexShader = glCreateShader(GL_VERTEX_SHADER);
		cubeFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		utility::CompileShader(cubeVertexShader, "resources/cubeVertex.glsl");
		utility::CompileShader(cubeFragmentShader, "resources/cubeFragment.glsl");

		cubeProgram = glCreateProgram();
		glAttachShader(cubeProgram, cubeVertexShader);
		glAttachShader(cubeProgram, cubeFragmentShader);
		utility::LinkShaderProgram(cubeProgram);

		glDetachShader(cubeProgram, cubeVertexShader);
		glDetachShader(cubeProgram, cubeFragmentShader);
		glDeleteShader(cubeVertexShader);
		glDeleteShader(cubeFragmentShader);

		cubePositionLocation = glGetAttribLocation(shaderProgram, "vPosition");

		glGenVertexArrays(1, &cubeVao);
		glBindVertexArray(cubeVao);

		glGenBuffers(1, &cubeVbo);
		glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertexData), cubeVertexData, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(cubePositionLocation);
		glVertexAttribPointer(cubePositionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	void Display() {
		currentTime= glfwGetTime();
		timeDelta = currentTime - lastTime;
		lastTime = currentTime;

		RotateCameraAroundObject();

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(shaderProgram);
		glBindVertexArray(vao);

		glm::vec4 worldRight = glm::inverse(view) * glm::vec4(1, 0, 0, 0);
		glm::vec4 worldUp = glm::inverse(view) * glm::vec4(0, 1, 0, 0);

		glUniform3f(CameraRight_worldspace_ID, worldRight.x, worldRight.y, worldRight.z);
		glUniform3f(CameraUp_worldspace_ID, worldUp.x, worldUp.y, worldUp.z);



		glUniform3f(BillboardPosID, 0.0f, 1.0f, 0.0f);
		glUniform2f(BillboardSizeID, 0.3f, 0.3f); 

		glUniform3f(colorLocation, 1, 0, 0);
		glUniformMatrix4fv(vpLocation, 1, GL_FALSE, glm::value_ptr(vp));

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		// cube stuff
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDisable(GL_BLEND);
		glUseProgram(cubeProgram);
		glBindVertexArray(cubeVao);
		glm::mat4 cubeModelMatrix(1.0f);
		cubeModelMatrix = glm::scale(cubeModelMatrix, glm::vec3(0.2f, 0.2f, 0.2f));
		glm::mat4 cubeMVP = projection * view * cubeModelMatrix;
		glUniformMatrix4fv(cubeMVPLocation, 1, GL_FALSE, glm::value_ptr(cubeMVP));
		glDrawArrays(GL_TRIANGLES, 0, 12 * 3);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	void Dispose() {
		glUseProgram(0);
		glDeleteProgram(shaderProgram);

		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
	}

}